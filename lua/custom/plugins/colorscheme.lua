return {
  {
    'catppuccin/nvim',
    lazy = false,
    name = 'catppuccin',
    opts = {
      flavour = 'mocha',
      color_overrides = {
        mocha = {
          base = '#11111B',
        },
        macchiato = {
          base = '#222429',
        },
      },
      transparent_background = true,
      integrations = {
        alpha = true,
        cmp = true,
        flash = true,
        gitsigns = true,
        illuminate = true,
        indent_blankline = { enabled = true },
        lsp_trouble = true,
        mason = true,
        mini = true,
        native_lsp = {
          enabled = true,
          virtual_text = {
            errors = { 'italic' },
            -- hints = { "italic" },
            -- warnings = { "italic" },
            information = { 'italic' },
            ok = { 'italic' },
          },
          underlines = {
            errors = { 'underline' },
            hints = { 'underdotted' },
            warnings = { 'undercurl' },
            information = { 'bold' },
            ok = { 'underline' },
          },
          inlay_hints = {
            background = true,
          },
        },
        navic = { enabled = true, custom_bg = 'lualine' },
        neotest = true,
        notify = true,
        semantic_tokens = true,
        telescope = true,
        treesitter = true,
        which_key = true,
      },
    },
  },

  { 'Mofiqul/dracula.nvim' },
}
